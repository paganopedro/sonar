from setuptools import setup, find_packages

setup(
    name='indicium_airflow_utils',
    package_dir={'plugins': 'plugins'},
    packages=find_packages(include=[
        'plugins',
        'plugins.*'
    ]),
    version='0.1.44',
    description='Python library for dag automation',
    author='Indicium Tech',
    license='MIT',
    package_data={'plugins': ['schemas/*']},
    install_requires=[
        'apache-airflow==1.10.09',
        'psycopg2-binary==2.8.4'
    ],
    entry_points={
        'airflow.plugins': [
            'singer_operator = \
                plugins.singer_operator:SingerPlugin',
            'embulk_operator = \
                plugins.embulk_operator:EmbulkPlugin'
        ]
    }
)
