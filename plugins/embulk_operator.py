import logging
import yaml
from base64 import b64encode

from airflow.operators.docker_operator import DockerOperator
from airflow.contrib.operators.ecs_operator import ECSOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.aws_hook import AwsHook

# This is where the docker file writes the config file to
EMBULK_DOCKER_CONFIG_FILE = '/tmp/config.yml'

log = logging.getLogger(__name__)


def add_extraction_date(config_dict):
    """Add extraction date to the Embulk configurations dictionary

    :param config_dict: Dictionary contatining Embulk configurations
    :type config_dict: dict
    :return: Embulk configurations dictionary containing extraction date
    :rtype: dict
    """
    add_time_filter = {
        'type': 'add_time',
        'to_column': {
            'name': 'ind_extraction_date',
            'type': 'timestamp'
        },
        'from_value': {
            'mode': 'upload_time'
        }
    }

    if 'filters' in config_dict and isinstance(config_dict['filters'], list):
        config_dict['filters'].append(add_time_filter)
    else:
        config_dict['filters'] = [add_time_filter]

    return config_dict


class EmbulkECSOperator(ECSOperator):
    """Run Embulk inside EC2 Container Service

    :param config: Embulk yaml configurations
    :type config: dict
    :param cluster: Cluster name on EC2 Container Service
    :type cluster: str
    :param aws_conn_id: Connection id of AWS credentials name. It's highly
        recommended to fill this parameter with an environment variable.
    :type aws_conn_id: str
    :param launch_type: Launch type on which to run the task, defaults to EC2
    :type launch_type: str, optional
    :param add_extraction_date: If True, add extraction date to embulk config,
        defaults to True.
    :type add_extraction_date: bool, optional
    :param task_definition: The task definition name on EC2 Container Service
    :type task_definition: str
    :param task_id: Name to be set to the task
    :type task_id: str
    :param dag: Reference to the dag the task is attached to
    :type dag: airflow.models.DAG
    :param awslogs_group: The CloudWatch group where the ECS container logs \
        are stored. Only required if you want logs to be shown in the \
        Airflow UI after your job has finished.
    :type awslogs_group: str
    :param awslogs_region: Region in which the CloudWatch logs are stored. If\
        None, this is the same as the region_name parameter. If that is also \
        None, this is the default AWS region based on your connection settings.
    :type awslogs_region: str
    """

    template_fields = ['config']
    task_definition = "embulk"

    @apply_defaults
    def __init__(self, config, cluster, aws_conn_id, launch_type='EC2',
                 add_extraction_date=True, *args, **kwargs):

        self.config = config
        self.add_extraction_date = add_extraction_date
        self.incremental_s3_config = kwargs.get('incremental_s3_config')

        super().__init__(
            cluster=cluster,
            task_definition=self.task_definition,
            launch_type=launch_type,
            aws_conn_id=aws_conn_id,
            overrides={},
            *args,
            **kwargs
        )

    def execute(self, context):

        if self.add_extraction_date:
            config = add_extraction_date(self.config)
        else:
            config = self.config

        yaml_bytes = yaml.dump(config).encode("utf-8")

        base64_config = b64encode(yaml_bytes)
        environment = {
            "BASE64_CONFIG": base64_config.decode("utf-8")
        }

        if self.incremental_s3_config is not None:
            conn = AwsHook(
                aws_conn_id=self.aws_conn_id
            ).get_connection(self.aws_conn_id)

            print(conn)

            environment['AWS_ACCESS_KEY_ID'] = conn.login
            environment['AWS_SECRET_ACCESS_KEY'] = conn.password
            environment['INCREMENTAL'] = 'true'
            environment['EMBULK_S3_STATE_BUCKET'] = \
                self.incremental_s3_config['s3_bucket']
            environment['EMBULK_S3_STATE_KEY'] = \
                self.incremental_s3_config['s3_key']

        env = [{"name": k, "value": v} for k, v in environment.items()]
        overrides = {
            'containerOverrides': [{
                'name': self.task_definition,
                'environment': env
            }]
        }

        self.overrides = overrides
        super(EmbulkECSOperator, self).execute(context)


class EmbulkDockerOperator(DockerOperator):
    """Execute embulk docker image

    :param config: Dictionary containing Embulk yaml configurations
    :type config: dict
    :param image: Image name in techindicium docker hub, defaults
        to techindicium/repos
    :type image: str, optional
    :param image_version: Image version from techindicium docker hub, defaults
        to embulk-0.2
    :type image_version: str, optional
    :param network_mode: Network mode type to execute Docker and connect
        containers
    :type network_mode: str
    :param task_id: Name to be set to the task
    :type task_id: str
    :param dag: Reference to the dag the task is attached to
    :type dag: airflow.models.DAG
    :param incremental_s3_config: If anything besides None, consider the data
        as static and apply incremental method to it's id
    :type incremental_s3_config: any
    """

    template_fields = ['config']

    @apply_defaults
    def __init__(self, config, image='techindicium/repos',
                image_version='embulk-0.2', network_mode='host',
                add_extraction_date=True, *args, **kwargs):

        self.config = config
        self.add_extraction_date = add_extraction_date
        self.aws_conn_id = kwargs.get('aws_conn_id')
        self.incremental_s3_config = kwargs.get('incremental_s3_config')

        super(EmbulkDockerOperator, self).__init__(
            image=f'{image}:{image_version}',
            network_mode=network_mode,
            *args,
            **kwargs
        )

    def execute(self, context):

        if self.add_extraction_date:
            config = add_extraction_date(self.config)
        else:
            config = self.config

        yaml_bytes = yaml.dump(config).encode("utf-8")

        base64_config = b64encode(yaml_bytes)
        environment = {
            "BASE64_CONFIG": base64_config.decode("utf-8")
        }

        if self.incremental_s3_config is not None:
            conn = AwsHook(
                aws_conn_id=self.aws_conn_id
            ).get_connection(self.aws_conn_id)
            environment['AWS_ACCESS_KEY_ID'] = conn.login
            environment['AWS_SECRET_ACCESS_KEY'] = conn.password
            environment['INCREMENTAL'] = 'true'
            environment['EMBULK_S3_STATE_BUCKET'] = \
                self.incremental_s3_config['s3_bucket']
            environment['EMBULK_S3_STATE_KEY'] = \
                self.incremental_s3_config['s3_key']

        self.environment = environment
        super(EmbulkDockerOperator, self).execute(context)


class EmbulkPlugin(AirflowPlugin):
    name = "embulk_operator"
    operators = [EmbulkDockerOperator, EmbulkECSOperator]
    # A list of class(es) derived from BaseHook
    hooks = []
    # A list of class(es) derived from BaseExecutor
    executors = []
    # A list of references to inject into the macros namespace
    macros = []
    # A list of objects created from a class derived
    # from flask_admin.BaseView
    admin_views = []
    # A list of Blueprint object created from flask.Blueprint
    flask_blueprints = []
    # A list of menu links (flask_admin.base.MenuLink)
    menu_links = []
