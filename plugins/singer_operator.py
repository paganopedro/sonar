import logging
import json
import os
import base64
from datetime import datetime

from airflow.operators.bash_operator import BashOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

base_dir = '/'.join(os.path.realpath(__file__).split('/')[:-1])

log = logging.getLogger(__name__)


def create_tmp_file(prefix):
    """Create a path to the file where taps and targets will be stored

    :param prefix: Desired file name contaning at least tap_name, client
        and execution_date
    :type prefix: str
    :return: file path
    :rtype: str
    """

    path = "/tmp/{}.json".format(prefix)
    return path


def create_tmp_data_file(prefix, client, execution_date):
    """Create the file where the data of the given date will be stored

    :param prefix: Tap name
    :type prefix: str
    :param client: Client name
    :type client: str
    :param execution_date: Date in which the program is executed,
        format YYYY-MM-DD (%Y-%m-%d if using datetime)
    :type execution_date: str
    :return: Data file path
    :rtype: str
    """

    path = "/tmp/data-{}{}{}.json".format(prefix, client, execution_date)
    print("Writing temp data to: {}".format(path))
    return path


class TargetPostgresOperator(BashOperator):
    """Send tap data to Postgres

    :param tap_name: Tap name in which the target will get the data
    :type tap_name: str
    :param client: Client name
    :type client: str
    :param pg_port: Postgres port id. It's highly recommended to
        fill this parameter with an environment variable when calling
        the object
    :type pg_port: str
    :param pg_host: Postgres host id. It's highly recommended to
        fill this parameter with an environment variable when calling
        the object
    :type pg_host: str
    :param pg_db: Postgres database id. It's highly recommended to
        fill this parameter with an environment variable when calling
        the object
    :type pg_db: str
    :param pg_user: Postgres user id. It's highly recommended to
        fill this parameter with an environment variable when calling
        the object
    :type pg_user: str
    :param pg_password: Postgres user password. It's highly recommended to
        fill this parameter with an environment variable
    :type pg_password: str
    :param pg_schema: Postgres schema in which the data will be stored inside \
        the database.
    :type pg_schema: str, optional
    :param image: Image name in techindicium docker hub, defaults
        to techindicium/repos
    :type image: str, optional
    :param image_version: Image version from techindicium docker hub, defaults
        to 'target-postgres'
    :type image_version: str, optional
    :param base64: Parameter used to define if the docker will run with base64
        configurations or mapping volumes, defaults to True
    :type base64: bool, optional
    """

    @apply_defaults
    def __init__(self, tap_name, client, pg_port, pg_host, pg_db, pg_user,
                 pg_password, pg_schema=None, image='techindicium/repos',
                 base64=True, image_version='target-postgres', *args,
                 **kwargs):

        self.tap_name = tap_name
        self.client = client
        self.image = image
        self.image_version = image_version
        self.pg_port = pg_port
        self.pg_host = pg_host
        self.pg_db = pg_db
        self.pg_user = pg_user
        self.pg_password = pg_password
        self.pg_schema = pg_schema
        self.base64 = base64

        super(TargetPostgresOperator, self).__init__(
            bash_command="will be replaced later", *args, **kwargs
        )

    def execute(self, context):
        """Function executed when the operator is called

        :param context: Dictionary containing references to the object.
            You can check the keys in
            https://airflow.apache.org/docs/stable/macros-ref
        :type context: dict
        """

        schema = self.pg_schema if self.pg_schema is not None else self.client

        conf = {
            "postgres_host": self.pg_host,
            "postgres_port": self.pg_port,
            "postgres_database": self.pg_db,
            "postgres_username": self.pg_user,
            "postgres_password": self.pg_password,
            "postgres_schema": schema
        }

        execution_date = datetime.strptime(
            context['ds'], "%Y-%m-%d"
        ).date().isoformat()

        # Set the date key as the past day
        conf['date'] = execution_date

        conf_location = self.write_target_config(conf, execution_date)
        data_file = create_tmp_data_file(
            self.tap_name,
            self.client,
            execution_date
        )
        if not os.path.isfile(data_file):
            # exit if file does not exist, tap creates file even if empty
            raise ValueError('No file for {}'.format(execution_date))

        # Define the data configuration file in base64
        with open(conf_location, 'r') as f:
            s = json.dumps(json.load(f))
            conf_file = base64.b64encode(s.encode('utf-8'))
            b64_conf_file = conf_file.decode()

        if self.base64:
            target_cmd = 'cat {data_file} | docker run -i --network host \
                         -e B64_CONF_FILE={b64_conf_file} \
                         -e B64_CONFIG=true \
                         {image}:{image_version}'.format(
                             data_file=data_file,
                             b64_conf_file=b64_conf_file,
                             image=self.image,
                             image_version=self.image_version
                         )
        else:
            target_cmd = 'cat {data_file} | docker run -i --network host \
                         -v {data_file}:{data_file} \
                         -v {conf_location}:{conf_location} \
                         -e CONF_LOCATION={conf_location} \
                         {image}:{image_version}'.format(
                             data_file=data_file,
                             conf_location=conf_location,
                             image=self.image,
                             image_version=self.image_version
                         )

        self.bash_command = target_cmd
        super(TargetPostgresOperator, self).execute(context)

    def write_target_config(self, conf, execution_date):
        """Create a file containing the tap configurations and return
            it's path

        :param conf: Dictionary containing Postgres configurations
        :type conf: dict
        :param execution_date: String in ISO 8601 date format of the
            date when the process is executed
        :type execution_date: str
        :return: Path where the Postgres configurations were written
        :rtype: str
        """

        log.info("Creating postgres-target conf")
        path = create_tmp_file("postgres-target-{}{}{}".format(
            self.tap_name.replace('-', '_'),
            self.client,
            execution_date
        )
        )

        with open(path, "w") as f:
            json.dump(conf, f)

        print("config for target v2 day created at path {}".format(path))

        return path


class TargetRedshiftOperator(BashOperator):
    """Load tap data to Redshift

    :param tap_name: Tap name in which the target will get the data
    :type tap_name: str
    :param client: Client name
    :type client: str
    :param image: Image name in techindicium docker hub, defaults
        to techindicium/repos
    :type image: str, optional
    :param image_version: Image version from techindicium docker hub, defaults
        to 'target-postgres'
    :type image_version: str, optional
    :param aws_key_id: AWS key id. It's highly recommended to
        fill this parameter with an environment variable
    :type aws_key_id: str, optional
    :param aws_secret_key: AWS secret access key. It's highly recommended to
        fill this parameter with an environment variable
    :type aws_secret_key: str, optional
    :param target_conf: Dictionary containing necessary redshift configurations
        such as host, port, database name, usernama, password, bucket and aws
        credentials, defaults to None
    :type target_conf: dict, optional
    :param db_host: Aws database host
    :type db_host: str, optional
    :param db_port: Aws database port
    :type db_port: str, optional
    :param db_name: Aws database name
    :type db_name: str, optional
    :param db_user: Aws username
    :type db_user: str, optional
    :param db_password: Aws user password
    :type db_password: str, optional
    :param bucket: Aws bucket
    :type bucket: str, optional
    :param base64: Parameter used to define if the docker will run with base64
        configurations or mapping volumes, defaults to True
    :type base64: bool, optional
    """

    @apply_defaults
    def __init__(self, tap_name, client, target_conf=None, aws_key_id=None,
                 aws_secret_key=None, db_host=None, db_port=None, db_name=None,
                 db_user=None, db_password=None, bucket=None, base64=True,
                 image='techindicium/repos', image_version='target-redshift',
                 *args, **kwargs):

        self.tap_name = tap_name
        self.client = client
        self.image = image
        self.image_version = image_version
        self.aws_key_id = aws_key_id
        self.aws_secret_key = aws_secret_key
        self.base64 = base64
        self.target_conf = target_conf
        self.db_host = db_host
        self.db_port = db_port
        self.db_name = db_name
        self.db_user = db_user
        self.db_password = db_password
        self.bucket = bucket

        super(TargetRedshiftOperator, self).__init__(
            bash_command="will be replaced later", *args, **kwargs
        )

    def execute(self, context):
        """Function executed when the operator is called

        :param context: Dictionary containing references to the object.
            You can check the keys in
            https://airflow.apache.org/docs/stable/macros-ref
        :type context: dict
        """

        if self.target_conf is not None:
            target_conf = self.target_conf
        else:
            target_conf = {
                "redshift_host": self.db_host,
                "redshift_port": self.db_port,
                "redshift_database": self.db_name,
                "redshift_username": self.db_user,
                "redshift_password": self.db_password,
                "redshift_schema": self.client,
                "default_column_lenght": 1000,
                "target_s3": {
                    "aws_access_key_id": self.aws_key_id,
                    "aws_secret_access_key": self.aws_secret_key,
                    "bucket": self.bucket,
                    "key_prefix": "__tmp"
                }
            }

        execution_date = datetime.strptime(
            context['ds'], "%Y-%m-%d"
        ).date().isoformat()

        # if no date specified in conf, the file will be uploaded for the past day
        if "date" not in target_conf and "line_date_field" not in target_conf:
            target_conf['date'] = execution_date

        conf_location = self.write_target_config(target_conf, execution_date)
        data_file = create_tmp_data_file(
            self.tap_name,
            self.client,
            execution_date
        )

        if not os.path.isfile(data_file):
            # exit if file does not exist, tap creates file even if empty
            raise ValueError('No file for {}'.format(execution_date))

        # Define the base64 data configuration file
        with open(conf_location, 'r') as f:
            s = json.dumps(json.load(f))
            conf_file = base64.b64encode(s.encode('utf-8'))
            b64_conf_file = conf_file.decode()

        if self.base64:
            target_cmd = 'cat {data_file} | docker run -i --network host \
                         -e B64_CONF_FILE={b64_conf_file} \
                         -e AWS_ACCESS_KEY_ID={aws_key_id} \
                         -e AWS_SECRET_ACCESS_KEY={aws_secret_key} \
                         -e B64_CONFIG=true \
                         {image}:{image_version}'.format(
                             data_file=data_file,
                             b64_conf_file=b64_conf_file,
                             aws_key_id=self.aws_key_id,
                             aws_secret_key=self.aws_secret_key,
                             image=self.image,
                             image_version=self.image_version
                         )
        else:
            target_cmd = 'cat {data_file} | docker run -i --network host \
                         -v {data_file}:{data_file} \
                         -v {conf_location}:{conf_location} \
                         -e CONF_LOCATION={conf_location} \
                         -e AWS_ACCESS_KEY_ID={aws_key_id} \
                         -e AWS_SECRET_ACCESS_KEY={aws_secret_key} \
                         {image}:{image_version}'.format(
                             data_file=data_file,
                             conf_location=conf_location,
                             aws_key_id=self.aws_key_id,
                             aws_secret_key=self.aws_secret_key,
                             image=self.image,
                             image_version=self.image_version
                         )

        self.bash_command = target_cmd
        super(TargetRedshiftOperator, self).execute(context)

    def write_target_config(self, conf, execution_date):
        log.info("Creating redshift-target conf")
        path = create_tmp_file("redshift-target-{}{}{}".format(
            self.tap_name,
            self.client,
            execution_date
        )
        )

        with open(path, "w") as f:
            json.dump(conf, f)

        log.info(f"Config for target day created at path {path}")
        return path


class TargetOperator(BashOperator):
    """Send tap data to the given target

    :param target_conf: Configurations needed to access the target
    :type target_conf: dict
    :param tap_name: Tap name where data is extracted
    :type tap_name: str
    :param client: Client name
    :type client: str
    :param image_version: Image version from techindicium docker hub
    :type image_version: str
    :param aws_key_id: AWS key id. It's highly recommended to
        fill this parameter with an environment variable
    :type aws_key_id: str
    :param aws_secret_key: AWS secret access key. It's highly recommended to
        fill this parameter with an environment variable
    :type aws_secret_key: str
    :param image: Image name in techindicium docker hub, defaults
        to techindicium/repos
    :type image: str, optional
    :param base64: Parameter used to define if the docker will run with base64
        configurations or mapping volumes, defaults to True
    :type base64: bool, optional
    :param env: Parameter used to define extra env vars
    :type env: dict, optional
    """

    @apply_defaults
    def __init__(self, target_conf, tap_name, client, image_version,
                 aws_key_id=None, aws_secret_key=None, base64=True, extra_env={},
                 image='techindicium/repos', *args, **kwargs):

        self.target_conf = target_conf
        self.tap_name = tap_name
        self.client = client
        self.image = image
        self.image_version = image_version
        self.aws_key_id = aws_key_id
        self.aws_secret_key = aws_secret_key
        self.base64 = base64
        self.extra_env = extra_env
        super(TargetOperator, self).__init__(
            bash_command="will be replaced later", *args, **kwargs
        )

    def execute(self, context):
        """Function executed when the operator is called

        :param context: Dictionary containing references to the object.
            You can check the keys in
            https://airflow.apache.org/docs/stable/macros-ref
        :type context: dict
        """

        conf = self.target_conf
        execution_date = datetime.strptime(
            context['ds'], "%Y-%m-%d"
        ).date().isoformat()

        # if no date specified in conf, the file will be upload
        # for the past day
        if "date" not in conf and "line_date_field" not in conf:
            conf['date'] = execution_date

        conf_location = self.write_target_config(conf, execution_date)
        data_file = create_tmp_data_file(
            self.tap_name,
            self.client,
            execution_date
        )
        if not os.path.isfile(data_file):
            # exit if file does not exist, tap creates file even if empty
            raise ValueError('No file for {}'.format(execution_date))

        # Encode conf_location to base64
        with open(conf_location, 'r') as f:
            s = json.dumps(json.load(f))
            conf_file = base64.b64encode(s.encode('utf-8'))
            b64_conf_file = conf_file.decode()

        # Format extra env as -e KEY=VALUE from env dict
        extra_env = " ".join(['-e {}={}'.format(k, v) for k, v in self.extra_env.items()])

        if self.base64:
            target_cmd = 'cat {data_file} | docker run -i --network host \
                         {extra_env} \
                         -e B64_CONF_FILE={b64_conf_file} \
                         -e AWS_ACCESS_KEY_ID={aws_key_id} \
                         -e AWS_SECRET_ACCESS_KEY={aws_secret_key} \
                         -e B64_CONFIG=true \
                         {image}:{image_version}'.format(
                             data_file=data_file,
                             b64_conf_file=b64_conf_file,
                             aws_key_id=self.aws_key_id,
                             aws_secret_key=self.aws_secret_key,
                             image=self.image,
                             image_version=self.image_version,
                             extra_env=extra_env
                         )
        else:
            target_cmd = 'cat {data_file} | docker run -i --network host \
                         -v {data_file}:{data_file} \
                         -v {conf_location}:{conf_location} \
                         {extra_env} \
                         -e CONF_LOCATION={conf_location} \
                         -e AWS_ACCESS_KEY_ID={aws_key_id} \
                         -e AWS_SECRET_ACCESS_KEY={aws_secret_key} \
                         {image}:{image_version}'.format(
                             data_file=data_file,
                             conf_location=conf_location,
                             aws_key_id=self.aws_key_id,
                             aws_secret_key=self.aws_secret_key,
                             image=self.image,
                             image_version=self.image_version,
                             extra_env=extra_env
                         )

        self.bash_command = target_cmd
        super(TargetOperator, self).execute(context)

    def write_target_config(self, conf, execution_date):
        """Create a file containing the target configurations and return
            it's path

        :param conf: Dictionary containing target configurations
        :type conf: dict
        :param execution_date: String in ISO 8601 date format of the
            date when the process is executed
        :type execution_date: str
        :return: Path where the target configurations were written
        :rtype: str
        """

        log.info("Creating target conf")
        path = create_tmp_file("target-{}{}{}".format(
            self.tap_name,
            self.client,
            execution_date
        )
        )

        with open(path, "w") as f:
            json.dump(conf, f)

        print("config for target v2 day created at path {}".format(path))
        return path


class TapOperator(BashOperator):
    """Extract data from the given tap

    :param tap_conf: Configurations needed to access the local where data will
        be extracted
    :type tap_conf: dict
    :param tap_name: Tap name where data will be extracted
    :type tap_name: str
    :param client: Client name
    :type client: str
    :param schema: JSON schema file name
    :type schema: str
    :param image_version: Image version from techindicium docker hub
    :type image_version: str
    :param image: Image name in techindicium docker hub, defaults
        to techindicium/repos
    :type image: str, optional
    :param base64: Parameter used to define if the docker will run with base64
        configurations or mapping volumes, defaults to True
    :type base64: bool, optional
    :param request: When tap has more than one schema, request is set to
        prepare each one separately, defaults to None
    :type request: str, optional
    :param set_dates: Auto set start_date and end_date, defaults
        to True for retro compatibility reasons
    :type execution_date: bool
    """

    template_fields = ['tap_conf']

    @apply_defaults
    def __init__(self, tap_conf, tap_name, client, schema, image_version,
                image='techindicium/repos', base64=True, request=None,
                set_dates=True, *args, **kwargs):

        self.tap_conf = tap_conf
        self.tap_name = tap_name
        self.client = client
        self.image = image
        self.image_version = image_version
        self.schema = schema
        self.request = request
        self.base64 = base64
        self.set_dates = set_dates

        super(TapOperator, self).__init__(
            bash_command="will be replaced later",
            *args,
            **kwargs
        )

    def execute(self, context):
        """Function executed when the operator is called

        :param context: Dictionary containing references to the object.
            You can check the macro keys in
            https://airflow.apache.org/docs/stable/macros-ref
        :type context: dict
        """

        conf = self.tap_conf
        execution_date = context['ds']

        conf_location = self.write_tap_config(
            conf,
            execution_date,
            set_dates=self.set_dates
        )

        data_file = create_tmp_data_file(
            self.tap_name,
            self.client,
            execution_date
        )

        # Base64 schema
        with open(f'{base_dir}/schemas/{self.schema}', 'r') as f:
            s = json.dumps(json.load(f))
            b64_string = base64.b64encode(s.encode('utf-8'))
            b64_schema = b64_string.decode()

        # Config base64 location
        with open(conf_location, 'r') as f:
            s = json.dumps(json.load(f))
            conf_file = base64.b64encode(s.encode('utf-8'))
            b64_conf_file = conf_file.decode()

        print('base64 config file = ', b64_conf_file)

        if self.base64:
            tap_cmd = 'docker run -e B64_SCHEMA={b64_schema_file} \
                      -e B64_CONF_FILE={b64_tap_conf_file} \
                      -e CATALOG={base_dir}/schemas/{schema} \
                      -e REQUEST={request} -e B64_CONFIG=true \
                      {image}:{image_version} > {data_file} '.format(
                          b64_schema_file=b64_schema,
                          b64_tap_conf_file=b64_conf_file,
                          base_dir=base_dir,
                          schema=self.schema,
                          request=self.request,
                          image=self.image,
                          image_version=self.image_version,
                          data_file=data_file
                      )
        else:
            tap_cmd = 'docker run -v {schema_path}:/opt/app/config/ \
                      -v {conf_location}:{conf_location} \
                      -e CONF_LOCATION={conf_location} \
                      -e SCHEMA_FILE=/opt/app/config/{schema} \
                      {image}:{image_version} > {data_file}'.format(
                          schema_path=f'{base_dir}/schemas/{self.schema}',
                          conf_location=conf_location,
                          schema=self.schema,
                          image=self.image,
                          image_version=self.image_version,
                          data_file=data_file
                      )

        self.bash_command = tap_cmd
        super(TapOperator, self).execute(context)

    def write_tap_config(self, conf, execution_date, set_dates):
        """Create a file containing the tap configurations and return
            it's path

        :param conf: Tap configurations set in the tap_conf parameter of
            the class
        :type conf: dict
        :param execution_date: String in ISO 8601 date format of the
            date when the process is executed
        :type execution_date: str
        :return: Path where the tap configurations were written
        :rtype: str

        :return: Path where the tap configurations were written
        :rtype: str
        """

        log.info("Creating tap conf")
        if(set_dates):
            conf['start_date'] = execution_date
            conf['end_date'] = execution_date
        print(conf)

        path = create_tmp_file("{}{}{}".format(
            self.tap_name,
            self.client,
            execution_date
        )
        )

        with open(path, "w") as f:
            json.dump(conf, f)

        print("config for day created at path {}".format(path))

        return path


class SingerPlugin(AirflowPlugin):
    name = "singer_operator"
    operators = [TapOperator, TargetOperator, TargetPostgresOperator]
    # A list of class(es) derived from BaseHook
    hooks = []
    # A list of class(es) derived from BaseExecutor
    executors = []
    # A list of references to inject into the macros namespace
    macros = []
    # A list of objects created from a class derived
    # from flask_admin.BaseView
    admin_views = []
    # A list of Blueprint object created from flask.Blueprint
    flask_blueprints = []
    # A list of menu links (flask_admin.base.MenuLink)
    menu_links = []
