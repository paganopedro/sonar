from airflow.models import Variable
from airflow.contrib.operators.ecs_operator import ECSOperator

ECS_SUBNET = Variable.get("ECS_SUBNET")
ECS_SECURITY_GROUP = Variable.get("ECS_SECURITY_GROUP")
ECS_CLUSTER = Variable.get("ECS_CLUSTER")
AWS_INDICIUM_CONN = Variable.get("AWS_INDICIUM_CONN")


def build_fargate_task(command_array, task_definition, task_id, dag, pool,
                       awslogs_group, awslogs_stream_prefix,
                       awslogs_region='us-east-1', env=None,
                       aws_conn_id=AWS_INDICIUM_CONN, public_ip='ENABLED',
                       subnet=ECS_SUBNET, security_group=ECS_SECURITY_GROUP,
                       cluster=ECS_CLUSTER, launch_type='FARGATE'
                       ):
    """Connect with AWS Fargate

    :param command_array: List of bash commands to be executed
    :type command_array: list
    :param task_definition: Task definition name on EC2 Container Service
    :type task_definition: str
    :param task_id: ECS task name
    :type task_id: str
    :param dag: DAG used to set Airflow workflow
    :type dag: airflow.models.DAG
    :param pool: Pool name set in the UI (Menu -> Admin -> Pools) used to
        limit execution parallelism on sets of tasks, defaults to None
    :type pool: str
    :param awslogs_group: CloudWatch group where ECS container logs are stored
    :type awslogs_group: str
    :param awslogs_stream_prefix: Prefix used for the CloudWatch logs
    :type awslogs_stream_prefix: str
    :param awslogs_region: Region set in AWS region parameter
    :type awslogs_region: str
    :param env: List containing dictionary with 'name' and 'value' of data
        warehouse, defaults to None
    :type env: list, optional
    :param aws_conn_id: Connection id of AWS credentials,
        defaults to AWS_INDICIUM_CONN
    :type aws_conn_id: str, optional
    :param public_ip: Set if the ip will be public or not. Values can be
        'ENABLED' or 'DISABLED', defaults to 'ENABLED'
    :type public_ip: str, optional
    :param subnet: Subnet used to configure ECS network, defaults to ECS_SUBNET
    :type subnet: str, optional
    :param security_group: Security group used to configure ECS network,
        defaults to ECS_SECURITY_GROUP
    :type security_group: str, optional
    :param cluster: Cluster name on ECS, ECS_CLUSTER
    :type cluster: str, optional
    :param launch_type: Launch type on which to run the task ('EC2'
        or 'FARGATE'), defaults to 'FARGATE'
    :type launch_type: str, optional
    :return: Execute a task on AWS EC2 Container Service
    :rtype: airflow.contrib.operators.ecs_operator.ECSOperator
    """

    # the marketing load dw version is set at the task definition

    if env is not None:
        env = [{"name": k, "value": v} for k, v in env.items()]
    else:
        env = []

    overrides = {
        'containerOverrides': [
            {
                'name': task_definition,
                'command': command_array,
                'environment': env
            }
        ]
    }

    network_configuration = {
        'awsvpcConfiguration': {
            'subnets': subnet,
            'securityGroups': security_group,
            'assignPublicIp': public_ip or 'ENABLED'
        }
    }

    if launch_type == 'FARGATE':
        task = ECSOperator(
            task_id="ECS_{}".format(task_id),
            task_definition=task_definition,
            cluster=cluster,
            overrides=overrides,
            pool=pool,
            network_configuration=network_configuration,
            launch_type=launch_type,
            awslogs_group=awslogs_group,
            awslogs_stream_prefix=awslogs_stream_prefix,
            awslogs_region=awslogs_region,
            dag=dag,
            aws_conn_id=aws_conn_id
        )
    else:
        task = ECSOperator(
            task_id="ECS_{}".format(task_id),
            task_definition=task_definition,
            cluster=cluster,
            overrides=overrides,
            pool=pool,
            launch_type=launch_type,
            awslogs_group=awslogs_group,
            awslogs_stream_prefix=awslogs_stream_prefix,
            dag=dag,
            aws_conn_id=aws_conn_id
        )

    return task
