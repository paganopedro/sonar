import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from datetime import datetime, timedelta

DEFAULT_CONNECTION_TIMEOUT = 30


class KondadoValidation:
    """Test if all elements in a selected list (streams) are extract by Kondado.
    return None if all streams are extract or raise Exception if something is
    different.

    :param kondado_key: Kondado API key
    :type kondado_key: str
    :param kondado_token: Kondado API token
    :type kondado_token: str
    :param streams: A list with all stream extracted from kondado.
    :type kondado_key: str
    :return: None or raise Exception if all streams are not extracted
    :type: None
    """

    def __init__(self, kondado_key: str, kondado_token: str, streams: list) -> None:

        self.base_url = "https://k2.kondado.com.br/"
        self.headers = {
            "accept": "application/json",
            "content-type": "application/json",
        }

        self.kondado_key = kondado_key
        self.kondado_token = kondado_token
        self.streams = streams

    def format_date(self, delta: int) -> str:

        run_date = datetime.strptime(self.run_date, "%Y-%m-%d")
        date = run_date + timedelta(days=delta)
        formated_date = datetime.strftime(date, "%Y-%m-%d")
        return formated_date

    def extract(self) -> dict:
        session = requests.Session()

        start_date = self.format_date(delta=-3)
        end_date = self.format_date(delta=2)
        print(f"Obtaining values between {start_date} and {end_date}")

        page_suffix = f"pipeline_logs?key={self.kondado_key}&"
        page_suffix += f"token={self.kondado_token}&"
        page_suffix += f"start_date={start_date}&"
        page_suffix += f"end_date={end_date}&page_size=50&page=1"

        url = self.base_url + page_suffix
        print(url)

        return self.do_request(url, session)

    def raise_if_not_200(self, response):
        if response.status_code != 200:
            message = str(response.status_code) + " " + str(response.text)
            raise Exception(message)
        else:
            return response.json()

    def do_request(self, url, session):
        raw_resp = self.requests_retry_session(session=session).get(
            url, headers=self.headers, timeout=DEFAULT_CONNECTION_TIMEOUT
        )

        response = self.raise_if_not_200(raw_resp)
        return response

    def requests_retry_session(
        self,
        retries=5,
        backoff_factor=2,
        status_forcelist=(500, 502, 504),
        session=None,
    ):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )

        adapter = HTTPAdapter(max_retries=retry)
        session.mount("http://", adapter)
        session.mount("https://", adapter)
        return session

    def validate_date(self, data: dict) -> None:
        if data.get("success") == True:  # noqa: E712
            print("Success extracting data from Kondado.")
        else:
            raise Exception("Extraction failed")

    def preprocess_data(self, data: dict) -> str:
        pipelines = data["data"]["data"]
        print(f"Data extracted: {pipelines}")
        success_pipeline = []

        for pipeline in pipelines:
            updated_date = pipeline["updatedAt"].split("T")[0]
            if updated_date == self.run_date:
                success_pipeline.append(pipeline["pipeline_name"])
                name = pipeline["pipeline_name"]
                time = pipeline["execution_time_seconds"]
                updatedAt = pipeline["updatedAt"]
                print(f"Stream {name} has executed in {time}s. Extract at: {updatedAt}")
        return success_pipeline

    def compare_data(self, success_pipeline: list) -> str:
        extra_extractions = list(set(success_pipeline) - set(self.streams))
        remaining_extractions = list(set(self.streams) - set(success_pipeline))

        if extra_extractions != []:
            print(f"Extra streams in Kondado Stream: {extra_extractions}")

        if remaining_extractions != []:
            print(f"Streams with data not ready: {remaining_extractions}.")

        print("All streams from Kondado were extracted.")

        return "DBT can now run."

    def test(self, *args, **kwargs) -> str:
        self.run_date = kwargs.get("run_date")
        print(f"Seleted date: {self.run_date}")
        data = self.extract()
        self.validate_date(data=data)
        success_pipeline = self.preprocess_data(data=data)
        result = self.compare_data(success_pipeline=success_pipeline)
        return result
