from datetime import timedelta
import os

from airflow import DAG
from plugins.embulk_operator import EmbulkDockerOperator
from plugins.singer_operator import TapOperator, \
    TargetPostgresOperator, TargetOperator, TargetRedshiftOperator
from airflow.utils.dates import days_ago

AWS_KEY_ID = os.environ.get('AWS_KEY_ID')
AWS_SECRET_KEY = os.environ.get('AWS_SECRET_KEY')

PG_USER = os.environ.get('PG_USER')
PG_PASSWORD = os.environ.get('PG_PASSWORD')
PG_PORT = os.environ.get('PG_PORT')
PG_DB = os.environ.get('PG_DB')
PG_HOST = os.environ.get('PG_HOST')

REDSHIFT_HOST = os.environ.get('REDSHIFT_HOST')
REDSHIFT_PORT = os.environ.get('REDSHIFT_PORT')
REDSHIFT_DB = os.environ.get('REDSHIFT_DB')
REDSHIFT_USER = os.environ.get('REDSHIFT_USER')
REDSHIFT_PASSWORD = os.environ.get('REDSHIFT_PASSWORD')
REDSHIFT_SCHEMA = os.environ.get('REDSHIFT_SCHEMA')
BUCKET = os.environ.get('REDSHIFT_BUCKET')

default_args = {
    'owner': 'Indicium',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['bruno.martins@indicium.tech'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(
    'airflow_singer_operator',
    default_args=default_args,
    description='DAG used to test new functionalities from \
        indicium_airflow_utils lib',
    schedule_interval=timedelta(days=1)
)

# tap_conf = {
#     'access_token': os.environ.get('FB_ACCESS_TOKEN'),
#     'account_id': os.environ.get('FB_ACCOUNT_ID'),
#     'page_id': os.environ.get('FB_PAGE_ID'),
#     'start_date': '2020-09-20',
#     'end_date': '2020-09-20'
# }
# tap_name = 'tap-fbpageinsights'
# client = 'indicium'
# schema = 'facebook-insights-schema.json'
# extraction_place = 'Facebook'

tap_conf = {
    'Authorization': os.environ.get('BLIP_AUTH'),
    'id': os.environ.get('BLIP_ID'),
    'schema': 'blip-details-report-schema',
    'start_date': '2020-10-21',
    'end_date': '2020-10-21',
    'tap_name': 'tap-blip'
}
schema = 'blip-details-report-schema.json'
tap_name = 'tap-blip'
client = 'brognoli'
extraction_place = 'Blip'


run_tap = TapOperator(tap_conf=tap_conf, tap_name=tap_name,
                      client=client, image_version=tap_name,
                      schema=schema, dag=dag, task_id=f'RunTap_{extraction_place}')

run_target_pg = TargetPostgresOperator(tap_name=tap_name, client=client,
                                       pg_port='5432', pg_host=PG_HOST,
                                       pg_db=PG_DB, pg_user=PG_USER,
                                       pg_password=PG_PASSWORD,
                                       task_id=f'RunTarget_{extraction_place}',
                                       dag=dag)

s3_conf = {
    'bucket': 'indicium-airflow-utils-test',
    'prefix': 'fb-insights',
    'client': client
}

run_target_s3 = TargetOperator(target_conf=s3_conf, tap_name=tap_name,
                               client=client, image_version='target-s3-b64',
                               aws_key_id=AWS_KEY_ID,
                               aws_secret_key=AWS_SECRET_KEY,
                               task_id='RunTarget_S3', dag=dag)

embulk_config = {
    'in': {
        'type': 'postgresql',
        'host': os.environ.get('PG_HOST'),
        'port': os.environ.get('PG_PORT'),
        'user': os.environ.get('PG_USER'),
        'password': os.environ.get('PG_PASSWORD'),
        'database': os.environ.get('PG_DB'),
        'table': 'facebook_page_insights',
        'schema': 'public',
    },
    'out': {
        'type': 'postgresql',
        'host': os.environ.get('PG_HOST'),
        'port': os.environ.get('PG_PORT'),
        'user': os.environ.get('PG_USER'),
        'password': os.environ.get('PG_PASSWORD'),
        'database': os.environ.get('PG_DB'),
        'table': 'facebook_page_insights',
        'schema': 'embulk',
        'mode': 'replace',
        'column_options': {
            'page_impressions_by_locale_unique': {
                'type': 'VARCHAR(2000)'
            }
        }
    }
}

run_embulk = EmbulkDockerOperator(
    task_id='RunEmbulkOperator',
    config=embulk_config,
    dag=dag
)

redshift_conf = {
    "redshift_host": REDSHIFT_HOST,
    "redshift_port": REDSHIFT_PORT,
    "redshift_database": REDSHIFT_DB,
    "redshift_username": REDSHIFT_USER,
    "redshift_password": REDSHIFT_PASSWORD,
    "redshift_schema": REDSHIFT_SCHEMA,
    "default_column_lenght": 100000,
    "target_s3": {
        "aws_access_key_id": AWS_KEY_ID,
        "aws_secret_access_key": AWS_SECRET_KEY,
        "bucket": BUCKET,
        "key_prefix": "__tmp"
    }
}

run_target_redshift = TargetRedshiftOperator(
    task_id='RunRedshiftOperator',
    tap_name=tap_name,
    client=client,
    target_conf=redshift_conf,
    dag=dag
)

run_tap >> run_target_pg >> run_embulk >> run_target_s3 >> run_target_redshift
