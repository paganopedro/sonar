from datetime import timedelta

from airflow import DAG
# from plugins import dbt

from airflow.utils.dates import days_ago

default_args = {
    'owner': 'Indicium',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['bruno.martins@indicium.tech'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(
    'airflow_utils',
    default_args=default_args,
    description='DAG used to test new functionalities from \
        indicium_airflow_utils lib',
    schedule_interval=timedelta(days=1),
)

# t1 = dbt.build_fargate_task()
