# Indicium Airflow Lib

### Version 0.1.44

--------------------------------

## Repository Structure
-----------------------------------

This project contains the following modules:
* /indicium_airflow: Module containing all library functions;
* /tests: It contains all the needed tests for the library

## Getting Started
-----------------------------------

This repository contains the Airflow functions used in the construction of new DAGs

### Setting up environment


1. Make sure you have [Python](https://www.python.org/downloads/) and [Pip](https://pip.pypa.io/en/stable/installing/) installed.
2. Execute `setup.py` to create the wheel file to import with `pip install` later.
```bash
$ python3 setup.py bdist_wheel
```
3. After executing the last command, a 'dist' folder containing the wheel file will be created in the directory. You can install it with the following command:
```bash
$ pip install /path/to/wheel_file.whl
```

### Importing library

Once you have installed the library, you can import it using:
```python
import indicium_airflow_utils
from indicium_airflow_utils import function
```

## Disclaimer
-------------------------------

Property of Indicium Tecnologia de Dados LTDA. All Rights Reserved. Unauthorized copying of this file, via any medium is strictly prohibited

Propriedade de Indicium Tecnologia de Dados LTDA. Todos os direitos reservados. Qualquer reprodução não-autorizada por qualquer meio é estritamente proibida.
