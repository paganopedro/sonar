#!/bin/sh

if [ -f /opt/embulk/plugins.txt ]; then
    for plugin in `cat /opt/embulk/plugins.txt`; do
        java -jar /opt/embulk/embulk.jar gem install $plugin
    done
fi


CONFIG_DIR='/tmp/config.yml'
echo "${@}"
if [ -z "$BASE64_CONFIG" ]
then
    # NOT BASE64
    echo "Running embulk without base64 config, a volume should be mapped"
    exec java $JAVA_OPTS -jar /opt/embulk/embulk.jar "${@}"
else
    # BASE64
    echo "Running embulk with base64 config, decoding string to $CONFIG_DIR"
    echo $BASE64_CONFIG | base64 -d > $CONFIG_DIR
    if [ -z "$INCREMENTAL" ]
    then
        # NOT INCREMENTAL
        exec java $JAVA_OPTS -jar /opt/embulk/embulk.jar run $CONFIG_DIR
    else 
        # INCREMENTAL
        # test s3 path set
        if [ -z "$EMBULK_S3_STATE_BUCKET" ]
        then
            echo 'EMBULK_S3_STATE_BUCKET not set'
            exit 1
        fi
        if [ -z "$EMBULK_S3_STATE_KEY" ]
        then
            echo 'EMBULK_S3_STATE_KEY not set'
            exit 1
        fi
        S3_DEST="$EMBULK_S3_STATE_BUCKET/$EMBULK_S3_STATE_KEY"
        
        # test access
        echo 'testuploadfile' > /tmp/test_upload_file
        echo 'testing s3 access'
        aws s3 cp /tmp/test_upload_file $S3_DEST/ || { echo 'S3 access failed' ; exit 1; }

        echo 'fetching state from s3'
        aws s3 cp $S3_DEST/state.yml state.yml || { echo 'State not found, starting from scracth' ; }

        echo "running INCREMENTAL and saving state to $S3_DEST/state.yml"
        java $JAVA_OPTS -jar /opt/embulk/embulk.jar run $CONFIG_DIR -c state.yml || { echo 'Embulk failed' ; exit 1; }


        echo "Uploading state to $S3_DEST/state.yml"
        aws s3 cp state.yml $S3_DEST/ || { echo 'S3 upload failed, the data load worked so you probably have dirty data at your embulk target' ; exit 1; }
    fi
fi
